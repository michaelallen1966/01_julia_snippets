# This is a comment

#=
This is a multi-line comment.
This is a multi line comment
=#

# println prints and adds new line

println("Hello World")

# print prints without new line
print("Hello ")
print("World")
print("\n")
